## Relationships

### Insert the following documents into a `users` collection

```
username : GoodGuyGreg
first_name : "Good Guy"
last_name : "Greg"
```
```
db.users.insertOne({username: "GoodGuyGreg", first_name:  "Good Guy", last_name: "Greg"})
```

```
username : ScumbagSteve
full_name :
  first : "Scumbag"
  last : "Steve"
```
```
db.users.insertOne({username: "ScumbagSteve", full_name: {first: "Scumbag", last: "Steve"}})
```

### Insert the following documents into a `posts` collection

```
username : GoodGuyGreg
title : Passes out at party
body : Wakes up early and cleans house
```
```
db.posts.insert({username:"GoodGuyGreg", title:"Passes out at party", body:"Raises your credit score"})
```

```
username : GoodGuyGreg
title : Steals your identity
body : Raises your credit score
```
```
db.posts.insert({ username:"GoodGuyGreg", title:"Steals your identity", body:"Raises your credit score"})
```

```
username : GoodGuyGreg
title : Reports a bug in your code
body : Sends you a Pull Request
```
```
db.posts.insert({username:"GoodGuyGreg", title:"Reports a bug in your code", body:"Sends you a pull request"})
```

```
username : ScumbagSteve
title : Borrows something
body : Sells it
```
```
db.posts.insert({ username:"ScumbagSteve", title:"Borrows something", body:"Sells it"})
```

```
username : ScumbagSteve
title : Borrows everything
body : The end
```
```
db.posts.insert({ username:"ScumbagSteve", title:"Borrows everything", body:"The end"})
```

```
username : ScumbagSteve
title : Forks your repo on github
body : Sets to private
```
```
db.posts.insert({username:"ScumbagSteve", title:"Forks your repo on github", body:"Sets to private"})
```

### Insert the following documents into a `comments` collection

```
username : GoodGuyGreg
comment : Hope you got a good deal!
post : [post_obj_id]
```

where [post_obj_id] is the ObjectId of the `posts` document: "Borrows something"

```
var post_oid_borrow_something = db.posts.findOne({title: "Borrows something"})

db.comments.insertOne({username: "GoodGuyGreg", comment: "Hope you got a good deal!", post: post_oid_borrow_something})
```


```
username : GoodGuyGreg
comment : What's mine is yours!
post : [post_obj_id]
```

where [post_obj_id] is the ObjectId of the `posts` document: "Borrows everything"

```
var post_oid_borrow_every = db.posts.findOne({title: "Borrows everything"})

db.comments.insertOne({username: "GoodGuyGreg", comment: "What's mine is yours!", post: post_oid_borrow_every})
```

```
username : GoodGuyGreg
comment : Don't violate the licensing agreement!
post : [post_obj_id]
```

where [post_obj_id] is the ObjectId of the `posts` document: "Forks your repo on github

```
var post_oid_forks = db.posts.findOne({title: "Forks your repo on github"})

db.comments.insertOne({username: "GoodGuyGreg", comment: "Don't violate the licensing agreement!", post: post_oid_forks})
```


```
username : ScumbagSteve
comment : It still isn't clean
post : [post_obj_id]
```

where [post_obj_id] is the ObjectId of the `posts` document: "Passes out at party"

```
var post_oid_pass = db.posts.findOne({title: "Passes out at party"})

db.comments.insertOne({username: "ScumbagSteve", comment: "It still isn't clean", post: post_oid_pass})
```


```
username : ScumbagSteve
comment : Denied your PR cause I found a hack
post : [post_obj_id]
```

where [post_obj_id] is the ObjectId of the `posts` document: "Reports a bug in your code"

```
var post_oid_report = db.posts.findOne({title: "Reports a bug in your code"})

db.comments.insertOne({username: "ScumbagSteve", comment: "Denied your PR cause I found a hack", post: post_oid_report})
```


## Querying related collections

1. find all users

2. find all posts

3. find all posts that was authored by "GoodGuyGreg"

4. find all posts that was authored by "ScumbagSteve"

5. find all comments

6. find all comments that was authored by "GoodGuyGreg"

7. find all comments that was authored by "ScumbagSteve"

8. find all comments belonging to the post "Reports a bug in your code"
```
db.comments.find({"post.title": "Reports a bug in your code"})
```


## Import a csv to mongodb
 
``` 
{ "_id" : "02906", "city" : "PROVIDENCE", "pop" : 31069, "state" : "RI", "capital" : { "name" : "Providence", "electoralCollege" : 4 } }
{ "_id" : "02108", "city" : "BOSTON", "pop" : 3697, "state" : "MA", "capital" : { "name" : "Boston", "electoralCollege" : 11 } }
{ "_id" : "10001", "city" : "NEW YORK", "pop" : 18913, "state" : "NY", "capital" : { "name" : "Albany", "electoralCollege" : 29 } }
{ "_id" : "01012", "city" : "CHESTERFIELD", "pop" : 177, "state" : "MA", "capital" : { "name" : "Boston", "electoralCollege" : 11 } }
{ "_id" : "32801", "city" : "ORLANDO", "pop" : 9275, "state" : "FL", "capital" : { "name" : "Tallahassee", "electoralCollege" : 29 } }
{ "_id" : "12966", "city" : "BANGOR", "pop" : 2867, "state" : "NY", "capital" : { "name" : "Albany", "electoralCollege" : 29 } }
{ "_id" : "32920", "city" : "CAPE CANAVERAL", "pop" : 7655, "state" : "FL", "capital" : { "name" : "Tallahassee", "electoralCollege" : 29 } }
{ "_id" : "NY", "name" : "New York", "pop" : 28300000, "state" : 1788 }
{ "_id" : "33125", "city" : "MIAMI", "pop" : 47761, "state" : "FL", "capital" : { "name" : "Tallahassee", "electoralCollege" : 29 } }
{ "_id" : "RI", "name" : "Rhode Island", "pop" : 1060000, "state" : 1790 }
{ "_id" : "MA", "name" : "Massachusetts", "pop" : 6868000, "state" : 1790 }
{ "_id" : "FL", "name" : "Florida", "pop" : 6800000, "state" : 1845 }
{ "_id" : "1", "name" : "Tom", "addresses" : [ "01001", "12997" ] }
{ "_id" : "02907", "city" : "CRANSTON", "pop" : 25668, "state" : "RI", "capital" : { "name" : "Providence", "electoralCollege" : 4 } }
{ "_id" : "2", "name" : "Bill", "addresses" : [ "01001", "12967", "32920" ] }
{ "_id" : "3", "name" : "Mary", "addresses" : [ "32801", "32920", "33125" ] }
{ "_id" : "12967", "city" : "NORTH LAWRENCE", "pop" : 943, "state" : "NY", "capital" : { "name" : "Albany", "electoralCollege" : 29 } }
{ "_id" : "01001", "city" : "AGAWAM", "pop" : 15338, "state" : "MA", "capital" : { "name" : "Boston", "electoralCollege" : 11 } }
{ "_id" : "12997", "city" : "WILMINGTON", "pop" : 958, "state" : "NY", "capital" : { "name" : "Albany", "electoralCollege" : 29 } }
```

mongoimport --db <database_name> --collection <collection_name> --file <drag file here>

```
Examples :

mongoimport --db usa --collection cities --file Downloads/cities.json
```

1. Show name and population of the cities where the population is over 10000
```
db.cities.find({$and: [{pop: {$gte:10000}}, {city: {$exists:true}}]}, {city:true, pop:true, _id:false})
result:
[
  { city: 'MIAMI', pop: 47761 },
  { city: 'CRANSTON', pop: 25668 },
  { city: 'AGAWAM', pop: 15338 },
  { city: 'NEW YORK', pop: 18913 },
  { city: 'PROVIDENCE', pop: 31069 }
]

OR

db.cities.find({$and: [{pop: {$gte:10000}}, {name: {$exists:true}}]}, {name:true, pop:true, _id:false})
result:
[
  { name: 'Rhode Island', pop: 1060000 },
  { name: 'Massachusetts', pop: 6868000 },
  { name: 'Florida', pop: 6800000 },
  { name: 'New York', pop: 28300000 }
]


```

2. Show the _id, city, name of the capital city of each state with a popultaion greater than 20,000.
```
db.cities.find({$and: [{pop: {$gte:20000}}, {capital: {$exists:true}}]}, {city:true, pop:true, "capital.name":true})

result:
[
  {
    _id: '33125',
    city: 'MIAMI',
    pop: 47761,
    capital: { name: 'Tallahassee' }
  },
  {
    _id: '02907',
    city: 'CRANSTON',
    pop: 25668,
    capital: { name: 'Providence' }
  },
  {
    _id: '02906',
    city: 'PROVIDENCE',
    pop: 31069,
    capital: { name: 'Providence' }
  }

```

